/*Copyright (c) 2015-2016 wavemaker.com All Rights Reserved.
 This software is the confidential and proprietary information of wavemaker.com You shall not disclose such Confidential Information and shall use it only in accordance
 with the terms of the source code license agreement you entered into with wavemaker.com*/
package com.hr_scaffold.hr.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.hr_scaffold.hr.Roles;
import com.hr_scaffold.hr.Users;


/**
 * ServiceImpl object for domain model class Roles.
 *
 * @see Roles
 */
@Service("HR.RolesService")
public class RolesServiceImpl implements RolesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RolesServiceImpl.class);

    @Autowired
	@Qualifier("HR.UsersService")
	private UsersService usersService;

    @Autowired
    @Qualifier("HR.RolesDao")
    private WMGenericDao<Roles, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<Roles, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "HRTransactionManager")
    @Override
	public Roles create(Roles roles) {
        LOGGER.debug("Creating a new Roles with information: {}", roles);
        Roles rolesCreated = this.wmGenericDao.create(roles);
        if(rolesCreated.getUserses() != null) {
            for(Users userse : rolesCreated.getUserses()) {
                userse.setRoles(rolesCreated);
                LOGGER.debug("Creating a new child Users with information: {}", userse);
                usersService.create(userse);
            }
        }
        return rolesCreated;
    }

	@Transactional(readOnly = true, value = "HRTransactionManager")
	@Override
	public Roles getById(Integer rolesId) throws EntityNotFoundException {
        LOGGER.debug("Finding Roles by id: {}", rolesId);
        Roles roles = this.wmGenericDao.findById(rolesId);
        if (roles == null){
            LOGGER.debug("No Roles found with id: {}", rolesId);
            throw new EntityNotFoundException(String.valueOf(rolesId));
        }
        return roles;
    }

    @Transactional(readOnly = true, value = "HRTransactionManager")
	@Override
	public Roles findById(Integer rolesId) {
        LOGGER.debug("Finding Roles by id: {}", rolesId);
        return this.wmGenericDao.findById(rolesId);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "HRTransactionManager")
	@Override
	public Roles update(Roles roles) throws EntityNotFoundException {
        LOGGER.debug("Updating Roles with information: {}", roles);
        this.wmGenericDao.update(roles);

        Integer rolesId = roles.getId();

        return this.wmGenericDao.findById(rolesId);
    }

    @Transactional(value = "HRTransactionManager")
	@Override
	public Roles delete(Integer rolesId) throws EntityNotFoundException {
        LOGGER.debug("Deleting Roles with id: {}", rolesId);
        Roles deleted = this.wmGenericDao.findById(rolesId);
        if (deleted == null) {
            LOGGER.debug("No Roles found with id: {}", rolesId);
            throw new EntityNotFoundException(String.valueOf(rolesId));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "HRTransactionManager")
	@Override
	public Page<Roles> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Roles");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "HRTransactionManager")
    @Override
    public Page<Roles> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Roles");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "HRTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service HR for table Roles to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "HRTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "HRTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "HRTransactionManager")
    @Override
    public Page<Users> findAssociatedUserses(Integer id, Pageable pageable) {
        LOGGER.debug("Fetching all associated userses");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("roles.id = '" + id + "'");

        return usersService.findAll(queryBuilder.toString(), pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service UsersService instance
	 */
	protected void setUsersService(UsersService service) {
        this.usersService = service;
    }

}

